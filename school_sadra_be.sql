-- -------------------------------------------------------------
-- TablePlus 5.4.0(504)
--
-- https://tableplus.com/
--
-- Database: school_sadra_be
-- Generation Time: 2023-08-19 22:12:51.2620
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."addresses";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS addresses_id_seq;

-- Table Definition
CREATE TABLE "public"."addresses" (
    "id" int8 NOT NULL DEFAULT nextval('addresses_id_seq'::regclass),
    "alamat" varchar(255),
    "kelurahan" varchar(255),
    "kecamatan" varchar(255),
    "kabupaten" varchar(255),
    "provinsi" varchar(255),
    "kode_pos" varchar(10),
    "user_id" int8 NOT NULL,
    "created_date" timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_date" timestamptz NOT NULL,
    "deleted_date" timestamptz,
    "created_by" varchar(255),
    "updated_by" varchar(255),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."files";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS files_id_seq;

-- Table Definition
CREATE TABLE "public"."files" (
    "id" int8 NOT NULL DEFAULT nextval('files_id_seq'::regclass),
    "name" varchar(255),
    "file_url" varchar(255),
    "created_date" timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_date" timestamptz NOT NULL,
    "deleted_date" timestamptz,
    "created_by" varchar(255),
    "updated_by" varchar(255),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."flyway_schema_history";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."flyway_schema_history" (
    "installed_rank" int4 NOT NULL,
    "version" varchar(50),
    "description" varchar(200) NOT NULL,
    "type" varchar(20) NOT NULL,
    "script" varchar(1000) NOT NULL,
    "checksum" int4,
    "installed_by" varchar(100) NOT NULL,
    "installed_on" timestamp NOT NULL DEFAULT now(),
    "execution_time" int4 NOT NULL,
    "success" bool NOT NULL,
    PRIMARY KEY ("installed_rank")
);

DROP TABLE IF EXISTS "public"."roles";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS roles_id_seq;

-- Table Definition
CREATE TABLE "public"."roles" (
    "id" int8 NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
    "created_date" timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_date" timestamptz NOT NULL,
    "deleted_date" timestamptz,
    "name" varchar(255) NOT NULL,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."students";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS students_id_seq;

-- Table Definition
CREATE TABLE "public"."students" (
    "id" int8 NOT NULL DEFAULT nextval('students_id_seq'::regclass),
    "full_name" varchar(255),
    "father_name" varchar(255),
    "mother_name" varchar(255),
    "user_id" int8 NOT NULL,
    "created_date" timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_date" timestamptz NOT NULL,
    "deleted_date" timestamptz,
    "created_by" varchar(255),
    "updated_by" varchar(255),
    "photo_url" varchar(255),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."user_roles";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS user_roles_id_seq;

-- Table Definition
CREATE TABLE "public"."user_roles" (
    "id" int8 NOT NULL DEFAULT nextval('user_roles_id_seq'::regclass),
    "created_date" timestamp NOT NULL,
    "updated_date" timestamp,
    "deleted_date" timestamp,
    "user_id" int8 NOT NULL,
    "role_id" int8 NOT NULL,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."users";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS users_id_seq;

-- Table Definition
CREATE TABLE "public"."users" (
    "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    "username" varchar(255) NOT NULL,
    "email" varchar(255) NOT NULL,
    "created_date" timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_date" timestamptz NOT NULL,
    "deleted_date" timestamptz,
    "last_login" timestamptz,
    "created_by" varchar(255),
    "updated_by" varchar(255),
    "password" varchar(255),
    "nim" varchar(50),
    PRIMARY KEY ("id")
);

INSERT INTO "public"."addresses" ("id", "alamat", "kelurahan", "kecamatan", "kabupaten", "provinsi", "kode_pos", "user_id", "created_date", "updated_date", "deleted_date", "created_by", "updated_by") VALUES
(1, 'Jl. Lebak bulus 2', 'Lebak Bulus', 'Pasar Minggu', 'Jakarta Selatan', 'DKI Jakarta', '12000', 6, '2023-08-19 21:55:39.368+07', '2023-08-19 21:55:39.368+07', NULL, 'Admin Sadra', 'Admin Sadra');

INSERT INTO "public"."files" ("id", "name", "file_url", "created_date", "updated_date", "deleted_date", "created_by", "updated_by") VALUES
(1, '1692455545252-Screenshot_2023-08-18_at_14.54.50.png', '/files/1692455545252-Screenshot_2023-08-18_at_14.54.50.png', '2023-08-19 21:32:26.642+07', '2023-08-19 21:32:26.642+07', NULL, 'Admin Sadra', 'Admin Sadra'),
(2, '1692456056788-Screenshot_2023-08-18_at_14.54.50.png', 'https://sadra.s3.ap-southeast-1.amazonaws.com/files/1692456056788-Screenshot_2023-08-18_at_14.54.50.png', '2023-08-19 21:40:58.112+07', '2023-08-19 21:40:58.112+07', NULL, 'Admin Sadra', 'Admin Sadra'),
(3, '1692456083820-Screenshot_2023-08-18_at_14.54.50.png', 'https://sadra.s3.ap-southeast-1.amazonaws.com/files/1692456083820-Screenshot_2023-08-18_at_14.54.50.png', '2023-08-19 21:41:24.912+07', '2023-08-19 21:41:24.912+07', NULL, 'Admin Sadra', 'Admin Sadra');

INSERT INTO "public"."flyway_schema_history" ("installed_rank", "version", "description", "type", "script", "checksum", "installed_by", "installed_on", "execution_time", "success") VALUES
(1, '20220914172807', 'CreateUserTable', 'SQL', 'V20220914172807__CreateUserTable.sql', -269197262, 'postgres', '2023-08-19 21:04:28.098818', 15, 't'),
(2, '20220914193823', 'CreateRoleTable', 'SQL', 'V20220914193823__CreateRoleTable.sql', 542179753, 'postgres', '2023-08-19 21:04:28.12255', 2, 't'),
(3, '20220914195825', 'CreateUserRoleTable', 'SQL', 'V20220914195825__CreateUserRoleTable.sql', -152094956, 'postgres', '2023-08-19 21:04:28.1296', 3, 't'),
(4, '20220915230749', 'AlterUserTableAddAudit', 'SQL', 'V20220915230749__AlterUserTableAddAudit.sql', 24461070, 'postgres', '2023-08-19 21:04:28.137857', 1, 't'),
(5, '20220917095028', 'AlterUserTableAddColumnPassword', 'SQL', 'V20220917095028__AlterUserTableAddColumnPassword.sql', -1954352763, 'postgres', '2023-08-19 21:04:28.142163', 1, 't'),
(6, '20220917130732', 'DumpRoleTable', 'SQL', 'V20220917130732__DumpRoleTable.sql', 1445620741, 'postgres', '2023-08-19 21:04:28.146704', 1, 't'),
(7, '20220924143338', 'AlterUserTableAddColumnNim', 'SQL', 'V20220924143338__AlterUserTableAddColumnNim.sql', 100388537, 'postgres', '2023-08-19 21:04:28.151321', 1, 't'),
(8, '20221002060149', 'CreateStudentTable', 'SQL', 'V20221002060149__CreateStudentTable.sql', -862029738, 'postgres', '2023-08-19 21:04:28.1554', 2, 't'),
(9, '20221002084224', 'AlterTableStudents', 'SQL', 'V20221002084224__AlterTableStudents.sql', -1900518790, 'postgres', '2023-08-19 21:04:28.161596', 1, 't'),
(10, '20221004204105', 'CreateAddressTable', 'SQL', 'V20221004204105__CreateAddressTable.sql', 562837484, 'postgres', '2023-08-19 21:04:28.165978', 2, 't'),
(11, '20221211174304', 'CreateFileStudentTable', 'SQL', 'V20221211174304__CreateFileStudentTable.sql', -1411470537, 'postgres', '2023-08-19 21:04:28.17192', 2, 't'),
(12, '20221211200825', 'AlterTableStudentAddPhotoUrl', 'SQL', 'V20221211200825__AlterTableStudentAddPhotoUrl.sql', -62950697, 'postgres', '2023-08-19 21:04:28.177406', 1, 't');

INSERT INTO "public"."roles" ("id", "created_date", "updated_date", "deleted_date", "name") VALUES
(1, '2023-08-19 21:04:28.146704+07', '2023-08-19 21:04:28.146704+07', NULL, 'ROLE_ADMIN'),
(2, '2023-08-19 21:04:28.146704+07', '2023-08-19 21:04:28.146704+07', NULL, 'ROLE_USER');

INSERT INTO "public"."students" ("id", "full_name", "father_name", "mother_name", "user_id", "created_date", "updated_date", "deleted_date", "created_by", "updated_by", "photo_url") VALUES
(1, 'Nabi Zadeh', 'Khumaid', 'Khanum Hiz', 6, '2023-08-19 21:55:39.372+07', '2023-08-19 21:55:39.372+07', NULL, 'Admin Sadra', 'Admin Sadra', 'https://sadra.s3.ap-southeast-1.amazonaws.com/files/1692455545252-Screenshot_2023-08-18_at_14.54.50.png');

INSERT INTO "public"."user_roles" ("id", "created_date", "updated_date", "deleted_date", "user_id", "role_id") VALUES
(2, '2023-08-19 21:08:47.535101', '2023-08-19 21:08:47.535101', NULL, 3, 1),
(3, '2023-08-19 21:16:45.624', '2023-08-19 21:16:45.624', NULL, 4, 1),
(4, '2023-08-19 21:17:18.432', '2023-08-19 21:17:18.432', NULL, 5, 1),
(5, '2023-08-19 21:55:39.375', '2023-08-19 21:55:39.375', NULL, 6, 2);

INSERT INTO "public"."users" ("id", "username", "email", "created_date", "updated_date", "deleted_date", "last_login", "created_by", "updated_by", "password", "nim") VALUES
(3, 'minayasa', 'minayasa@gmail.com', '2023-08-19 21:07:42.43299+07', '2023-08-19 21:07:42.43299+07', NULL, NULL, NULL, NULL, '$2a$10$sz6U7wn0MPTyj2OAgSWOrOYAecFxeXxVkM7bLLnS01ZFS8F2YUfZe', NULL),
(4, 'jawad', 'jawad@gmail.com', '2023-08-19 21:16:45.612+07', '2023-08-19 21:16:45.612+07', NULL, NULL, 'Admin Sadra', 'Admin Sadra', '$2a$10$YAKSXezah9dHucyOuXbuZefN0bgnGim1Jjg6M7zu141qXd29/rhii', ''),
(5, 'bagir', 'bagir@gmail.com', '2023-08-19 21:17:18.431+07', '2023-08-19 21:17:18.431+07', NULL, NULL, 'Admin Sadra', 'Admin Sadra', '$2a$10$jT6jjUl4VsRDTHNAHlq7RO3cDYpoHOeq251tXdCurfY.R0Z7XBazC', ''),
(6, 'zadeh', 'nabizadeh@gmail.com', '2023-08-19 21:55:39.343+07', '2023-08-19 22:04:24.881+07', NULL, NULL, 'Admin Sadra', 'Admin Sadra', '$2a$10$NGdQhKp4rDEYawc1aXv9VermlmbM3mEFYJkb2kMEowZJQAn1Nuy3C', '3343345');

ALTER TABLE "public"."addresses" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users"("id");
ALTER TABLE "public"."students" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users"("id");
ALTER TABLE "public"."user_roles" ADD FOREIGN KEY ("role_id") REFERENCES "public"."roles"("id");
ALTER TABLE "public"."user_roles" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users"("id");
