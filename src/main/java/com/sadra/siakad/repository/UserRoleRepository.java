package com.sadra.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sadra.siakad.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
