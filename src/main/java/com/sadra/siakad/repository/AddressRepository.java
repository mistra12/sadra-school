package com.sadra.siakad.repository;

import com.sadra.siakad.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
