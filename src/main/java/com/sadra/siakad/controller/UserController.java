package com.sadra.siakad.controller;

import com.sadra.siakad.payload.request.LoginRequest;
import com.sadra.siakad.payload.request.UserRequest;
import com.sadra.siakad.payload.response.JWTAuthResponse;
import com.sadra.siakad.payload.response.PageUserResponse;
import com.sadra.siakad.payload.response.UserResponse;
import com.sadra.siakad.security.JwtTokenProvider;
import com.sadra.siakad.service.UserService;
import com.sadra.siakad.utils.DataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/v1/users")
@CrossOrigin(origins = "*")
public class UserController {

    private UserService userService;
    private AuthenticationManager authenticationManager;
    private JwtTokenProvider tokenProvider;

    public UserController(UserService userService,
            AuthenticationManager authenticationManager,
            JwtTokenProvider tokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/welcome")
    public String welcome() {
        return "get";
    }

    @PostMapping("/login")
    public ResponseEntity<JWTAuthResponse> authenticateUser(@RequestBody LoginRequest request, Errors errors) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                request.getUsername(), request.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = tokenProvider.generateToken(authentication);

        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                System.out.println(error.getDefaultMessage());
            }

        }

        return ResponseEntity.ok(new JWTAuthResponse(token));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/register")
    public ResponseEntity<DataResponse<UserResponse>> createUser(@Valid @RequestBody UserRequest request,
            Errors errors) {
        DataResponse<UserResponse> response = new DataResponse<>();
        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                response.getMessages().add(error.getDefaultMessage());
            }
            response.setStatus(false);
            response.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        response.setStatus(true);
        response.setPayload(userService.createUser(request));
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/current")
    public ResponseEntity<UserResponse> currentUser(Principal principal) {
        return new ResponseEntity<>(userService.currentUser(principal.getName()), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<PageUserResponse> getAllUsers(
            @RequestParam(value = "email",  required = false) String email,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "nim", required = false) String nim,
            @RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
    	return ResponseEntity.ok(userService.getAllUsers(username, email, nim, pageNo, pageSize, sortBy, sortDir));
    }
}
