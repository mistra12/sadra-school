package com.sadra.siakad.service.impl;

import com.sadra.siakad.entity.Address;
import com.sadra.siakad.entity.Role;
import com.sadra.siakad.entity.Student;
import com.sadra.siakad.entity.User;
import com.sadra.siakad.entity.UserRole;
import com.sadra.siakad.payload.request.StudentRequest;
import com.sadra.siakad.payload.response.StudentResponse;
import com.sadra.siakad.repository.AddressRepository;
import com.sadra.siakad.repository.RoleRepository;
import com.sadra.siakad.repository.StudentRepository;
import com.sadra.siakad.repository.UserRepository;
import com.sadra.siakad.repository.UserRoleRepository;
import com.sadra.siakad.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public StudentResponse createStudent(StudentRequest request) {
        // cek role
        Optional<Role> role = roleRepository.findByName("ROLE_USER");
        if (role.isEmpty()) {
            System.out.println("Role not found");
        }
        // create user
        User user = modelMapper.map(request, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Student student = modelMapper.map(request, Student.class);
        student.setUser(user);
        user.setStudent(student);
        // create relation address
        Address address = modelMapper.map(request, Address.class);
        address.setUser(user);
        user.setAddress(address);
        userRepository.save(user);

        // create relation user_roles
        UserRole userRole = new UserRole();
        userRole.setRole(role.get());
        userRole.setUser(user);
        userRoleRepository.save(userRole);

        return response(user, student, address);
    }

    @Override
    public StudentResponse getDetailStudent(Long userId) {
        User user = userRepository.findById(userId).get();
        Student student = user.getStudent();
        Address address = user.getAddress();
        return response(user, student, address);
    }

    @Override
    public StudentResponse updateStudent(StudentRequest request, Long userId) {
        User user = userRepository.findById(userId).get();
        user.setEmail(request.getEmail());
        user.setNim(request.getNim());
        user.setUsername(request.getUsername());
        userRepository.save(user);

        Student student = user.getStudent();
        if (student == null) {
            Student newStudent = modelMapper.map(request, Student.class);
            newStudent.setUser(user);
            studentRepository.save(newStudent);
        } else {
            student.setFatherName(request.getFatherName());
            student.setMotherName(request.getMotherName());
            student.setFullName(request.getFullName());
            student.setPhotoUrl(request.getPhotoUrl());
            studentRepository.save(student);
        }

        Address address = user.getAddress();
        if (address == null) {
            Address newAddress = modelMapper.map(request, Address.class);
            newAddress.setUser(user);
            addressRepository.save(newAddress);
        } else {
            address.setAlamat(request.getAlamat());
            address.setKelurahan(request.getKelurahan());
            address.setKecamatan(request.getKecamatan());
            address.setKabupaten(request.getKabupaten());
            address.setProvinsi(request.getProvinsi());
            address.setKodePos(request.getKodePos());
            addressRepository.save(address);
        }

        return response(user, student,address);
    }

    private StudentResponse response(User user, Student student, Address address) {
        return StudentResponse.builder()
                .userId(user.getId())
                .email(user.getEmail())
                .nim(user.getNim())
                .username(user.getUsername())
                .fullName(student == null ? null : student.getFullName())
                .fatherName(student == null ? null : student.getFatherName())
                .motherName(student == null ? null : student.getMotherName())
                .alamat(address == null ? null : address.getAlamat())
                .kelurahan(address == null ? null : address.getKelurahan())
                .kecamatan(address == null ? null : address.getKecamatan())
                .kabupaten(address == null ? null : address.getKabupaten())
                .provinsi(address == null ? null : address.getProvinsi())
                .kodePos(address == null ? null : address.getKodePos())
                .photoUrl(student == null ? null : student.getPhotoUrl())
                .build();
    }

}
