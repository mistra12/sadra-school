package com.sadra.siakad.service.impl;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.sadra.siakad.entity.File;
import com.sadra.siakad.repository.FileRepository;
import com.sadra.siakad.service.AmazonS3Service;
import com.sadra.siakad.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class FileServiceImpl implements FileService {
    @Autowired
    AmazonS3Service amazonS3Service;

    @Autowired
    FileRepository fileRepository;

    @Value("${aws.s3.bucket.name}")
    private String bucketName;

    @Value("${aws.endpointUrl}")
    private String endpointUrl;

    @Override
    public String upload(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }

        String fileUrl = "";
        try {

            Map<String, String> metadata = new HashMap<>();
            metadata.put("Content-Type", file.getContentType());
            metadata.put("Content-Length", String.valueOf(file.getSize()));

            String path = String.format("%s/%s", bucketName, "files");

            java.io.File fileConv = convertMultiPartToFile(file);
            String fileName = generateFileName(file);
            fileUrl = endpointUrl + "/files/" + fileName;
            // Uploading file to s3
            PutObjectResult putObjectResult = amazonS3Service.upload(
                    path, fileName, Optional.of(metadata), file.getInputStream());
            // Saving metadata to db
            File fileSaved = fileRepository.save(File.builder().name(fileName).fileUrl(fileUrl).build());
            System.out.println(fileSaved.getId());
            fileConv.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileUrl;
    }

    private String generateFileName(MultipartFile multipartFile) {
        return new Date().getTime() + "-" +
                multipartFile.getOriginalFilename().replace(" ", "_");
    }

    private java.io.File convertMultiPartToFile(MultipartFile file) throws IOException {
        java.io.File convFile = new java.io.File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }


}
