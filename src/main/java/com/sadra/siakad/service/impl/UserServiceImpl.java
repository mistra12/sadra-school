package com.sadra.siakad.service.impl;

import com.sadra.siakad.entity.Role;
import com.sadra.siakad.entity.User;
import com.sadra.siakad.entity.UserRole;
import com.sadra.siakad.payload.request.UserRequest;
import com.sadra.siakad.payload.response.PageUserResponse;
import com.sadra.siakad.payload.response.UserResponse;
import com.sadra.siakad.repository.RoleRepository;
import com.sadra.siakad.repository.UserRepository;
import com.sadra.siakad.repository.UserRoleRepository;
import com.sadra.siakad.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserRoleRepository userRoleRepository;
    private ModelMapper modelMapper;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
            ModelMapper modelMapper,
            RoleRepository roleRepository,
            UserRoleRepository userRoleRepository,
            PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public UserResponse createUser(UserRequest request) {
        Optional<Role> role = roleRepository.findByName("ROLE_USER");
        if (role.isEmpty()) {
            throw new RuntimeException("Role Not Found");
        }
        User user = modelMapper.map(request, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User createdUser = userRepository.save(user);
        UserRole userRole = new UserRole();
        userRole.setRole(role.get());
        userRole.setUser(user);
        userRoleRepository.save(userRole);
        UserResponse response = modelMapper.map(createdUser, UserResponse.class);
        return response;
    }

    @Override
    public UserResponse currentUser(String email) {
        Optional<User> currentUser = userRepository.findByEmail(email);
        return modelMapper.map(currentUser.get(), UserResponse.class);
    }

    @Override
    public PageUserResponse getAllUsers(String username, String email, String nim, int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        
        // Create Page instance
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<User> users = userRepository.findAll(pageable);

        if (email != null || username != null || nim != null) {
            if (email == null) {
                email = "";
            }
            if(username == null) {
                username = "";
            }
            if (nim == null) {
                nim = "";
            }
            users = userRepository.getUserFilter(nim, username, email, pageable);
        }

        // get content for page object
        List<User> listUsers = users.getContent();
        
        List<UserResponse> content = listUsers.stream().map(user -> modelMapper.map(user, UserResponse.class)).collect(Collectors.toList());
        PageUserResponse response = PageUserResponse.builder()
        		.content(content)
        		.pageNo(pageNo)
        		.pageSize(pageSize)
        		.totalElement(pageSize)
        		.totalPages(users.getTotalPages())
        		.last(users.isLast())
        		.build();
        return response;
    }
}
