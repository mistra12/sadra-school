package com.sadra.siakad.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRequest {
    @NotEmpty(message = "field username is required")
    private String username;

    @NotEmpty(message = "field email is required")
    @Email
    private String email;

    @NotEmpty(message = "field password is required")
    private String password;

    @NotEmpty(message = "field nim is required")
    private String nim;
}
