create table user_roles
(
    id bigserial not null
        constraint user_roles_pkey
            primary key,
    created_date timestamp not null,
    updated_date timestamp,
    deleted_date timestamp,
    user_id bigint not null
        constraint user_roles_users_id_fk
            references users,
    role_id bigint not null
        constraint user_roles_roles_id_fk
            references roles
);