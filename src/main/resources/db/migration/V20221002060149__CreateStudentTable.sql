create table students(
    id bigserial not null
       constraint student_pk
        primary key,
    full_name varchar(255),
    father_name varchar(255),
    mother_name varchar (255),
    user_id bigint not null
        constraint students_users_id_fk
            references users,
    created_date timestamptz default current_timestamp not null,
    updated_date timestamptz not null,
    deleted_date timestamptz
);