create table roles (
    id bigserial not null
        constraint roles_pkey
            primary key,
    created_date timestamptz default current_timestamp not null,
    updated_date timestamptz not null,
    deleted_date timestamptz,
    name varchar(255) not null
);
