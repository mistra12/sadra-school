create table users(
    id bigserial not null
       constraint user_pk
        primary key,
    username varchar(255) unique not null,
    email varchar(255) unique not null,
    created_date timestamptz default current_timestamp not null,
    updated_date timestamptz not null,
    deleted_date timestamptz,
    last_login timestamptz
);